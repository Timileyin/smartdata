﻿namespace ElasticSearch.Data.Entities
{    
    public class Property:BaseEntity
    {
        public int? PropertyID { get; set; }
        public string FormerName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }
}
