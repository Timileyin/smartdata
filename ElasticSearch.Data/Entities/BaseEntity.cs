﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Data.Entities
{
    public class BaseEntity
    {        
        public string Name { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
    }
}
