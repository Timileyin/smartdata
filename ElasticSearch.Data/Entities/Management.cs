﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Data.Entities
{
    public class Management:BaseEntity
    {
        public int? MgmtID { get; set; }
    }
}
