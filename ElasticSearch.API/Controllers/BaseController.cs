﻿using ElasticSearch.API.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ElasticSearch.API.Controllers
{
    public class BaseController : ControllerBase
    {

        protected IActionResult PrepareResult(string message, HttpStatusCode statusCode, bool successful = false, object content = null)
        {
            return StatusCode((int)statusCode, new APIResponseModel(message, (int)statusCode, content, successful));
        }
    }
}
