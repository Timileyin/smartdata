﻿using ElasticSearch.Data.Entities;
using ElasticSearch.Repository.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ElasticSearch.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SearchController : BaseController 
    { 
        private readonly BaseRepository<Property> _propertyRepository;
        private readonly ILogger _logger;
        public SearchController(BaseRepository<Property> propertyRepository, ILogger<SearchController> logger)
        {
            _propertyRepository = propertyRepository;
            _logger = logger;
        }
        [HttpGet]
        public async Task<IActionResult> Index(string searchPhrase, [FromQuery] List<string> market, int limit = 25)
        {            
            var properties = await _propertyRepository.SearchAllIndices(searchPhrase, market, limit);
            return PrepareResult($"{properties.Count} items returned", HttpStatusCode.OK, true, properties);
        }
    }
}
