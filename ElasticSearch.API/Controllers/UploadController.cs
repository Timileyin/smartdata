﻿using ElasticSearch.API.Models;
using ElasticSearch.Data.Entities;
using ElasticSearch.Repository.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Management = ElasticSearch.Data.Entities.Management;
using Property = ElasticSearch.Data.Entities.Property;

namespace ElasticSearch.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UploadController : BaseController
    {
        private readonly BaseRepository<Property> _propertyRepository;
        private readonly BaseRepository<Management> _mgtRepository;
        private readonly ILogger _logger;
        public UploadController(BaseRepository<Property> propertyRepository, BaseRepository<Management> mgtRepository, ILogger<UploadController> logger)
        {
            _propertyRepository = propertyRepository;
            _mgtRepository = mgtRepository;
            _logger = logger;
        }

        [HttpPost("properties")]
        public async Task<IActionResult> Properties(IFormFile file)
        {
            if(file == null)
            {
                return PrepareResult("Please upload a file", HttpStatusCode.BadRequest);
            }

            if (!file.FileName.EndsWith(".json"))
            {
                return PrepareResult("Please upload a json file", HttpStatusCode.BadRequest);
            }

            if (file.Length > 0)
            {
                try
                {
                    var json = string.Empty;
                    using (var fileStream = new StreamReader(file.OpenReadStream()))
                    {
                        json = await fileStream.ReadToEndAsync();
                    }
                    _logger.LogInformation("Uploading documents in the app_property index");
                    var properties = JsonConvert.DeserializeObject<List<UploadedProperty>>(json);
                    var uploadedProperties = properties.Select(p => new Data.Entities.Property
                    {
                        City = p.Property.City,
                        FormerName = p.Property.FormerName,
                        PropertyID = p.Property.PropertyID,
                        Lat = p.Property.Lat,
                        Lng = p.Property.Lng,
                        Market = p.Property.Market,
                        Name = p.Property.Name,
                        State = p.Property.State,
                        StreetAddress = p.Property.StreetAddress
                    }).ToList();
                    var response = await _propertyRepository.CreateObjects(uploadedProperties);
                    _logger.LogInformation($"{response.Items.Count} out of {uploadedProperties.Count} uploaded successfully");
                    return PrepareResult($"{response.Items.Count} documents out of {uploadedProperties.Count} uploaded successfully", HttpStatusCode.OK, response.IsValid, null);
                }catch(Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return PrepareResult("An error occured while uploading file", HttpStatusCode.InternalServerError);
                }
            }
            return  PrepareResult("You uploaded an empty file", HttpStatusCode.BadRequest);
        }

        [HttpPost("management")]
        public async Task<IActionResult> Management(IFormFile file) 
        {

            if (file == null)
            {
                return PrepareResult("Please upload a file", HttpStatusCode.BadRequest);
            }

            if (!file.FileName.EndsWith(".json"))
            {
                return PrepareResult("Please upload a json file", HttpStatusCode.BadRequest);
            }

            if (file.Length > 0)
            {
                try
                {
                    var json = string.Empty;
                    using (var fileStream = new StreamReader(file.OpenReadStream()))
                    {
                        json = await fileStream.ReadToEndAsync();
                    }
                    _logger.LogInformation("Uploading documents in the app_management index");
                    var mgmt = JsonConvert.DeserializeObject<List<UploadedManagement>>(json);
                    var uploadedMgmt = mgmt.Select(m => new Data.Entities.Management
                    {
                        Market = m.Mgmt.Market,
                        Name = m.Mgmt.Name,
                        MgmtID = m.Mgmt.MgmtId,
                        State = m.Mgmt.State
                    }).ToList();
                    var response = await _mgtRepository.CreateObjects(uploadedMgmt);
                    _logger.LogInformation($"{response.Items.Count} out of {uploadedMgmt.Count} uploaded successfully");
                    return PrepareResult($"{response.Items.Count} out of {uploadedMgmt.Count} uploaded successfully", HttpStatusCode.OK, response.IsValid, null);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return PrepareResult("An error occured while uploading file", HttpStatusCode.InternalServerError);
                }
            }
            return PrepareResult("You uploaded an empty file", HttpStatusCode.BadRequest);
        }


    }
}
