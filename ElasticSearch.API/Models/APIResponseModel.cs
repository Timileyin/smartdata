﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.API.Models
{
    public class APIResponseModel
    {
        public object Content { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public bool Successful { get; set; }

        public APIResponseModel(string message, int statusCode, object content, bool successful)
        {
            Content = content;
            Message = message;
            StatusCode = statusCode;
            Successful = successful; 
        }
    }
}
