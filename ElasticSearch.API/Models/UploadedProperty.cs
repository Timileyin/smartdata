﻿namespace ElasticSearch.API.Models
{
    public class UploadedProperty
    {
        public Property Property { get; set; }
    }

    public class Property
    {
        public string Market { get; set; }
        public string State { get; set; }
        public int PropertyID { get; set; }
        public string Name { get; set; }
        public string FormerName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }

    public class UploadedManagement
    {
        public Management Mgmt { get; set; }
    }

    public class Management
    {
        public int MgmtId { get; set; }
        public string Market { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
    }
}
