﻿using Elasticsearch.Net;
using ElasticSearch.Data.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Threading.Tasks;

namespace ElasticSearch.API.Extensions
{
    public static class ElasticSearchExtension
    {
        public static void AddElasticSearchClient(this IServiceCollection services, IConfiguration configuration, ILogger logger)
        {
            var searchDefaultIndex = configuration["ElasticSearch:DefaultIndex"];
            var searchURL = configuration["ElasticSearch:URL"];
            var pool = new SingleNodeConnectionPool(new Uri(searchURL));
            var settings = new ConnectionSettings(pool).DefaultIndex(searchDefaultIndex).ThrowExceptions();
            ElasticClient client = new ElasticClient(settings);
            services.AddSingleton(client);

            Task.Run(() => CreateIndex<Property>(client, logger));            
            Task.Run(() => CreateIndex<Management>(client, logger));
        }
        
        private static async Task<CreateIndexResponse> CreateIndex<T>(IElasticClient client, ILogger logger) where T:BaseEntity
        {
            var index = $"app_{typeof(T).Name.ToLower()}";
            if (!client.Indices.Exists(index).Exists)
            {
                try
                {
                    var createIndexResponse = await client.Indices.CreateAsync(index,

                    index => index.Settings(s => s.Analysis(a => a.Analyzers(aa => aa
                                .Custom("index_question", ca => ca
                                    .Tokenizer("standard")
                                    .Filters("lowercase", "stop", "porter_stem", "english_stop")
                                )
                                .Custom("search_question", ca => ca
                                    .Tokenizer("standard")
                                    .Filters("lowercase", "stop", "porter_stem", "english_stop")
                                )
                            ).TokenFilters(s => s
                                .Stop("english_stop", s => s
                                    .StopWords("_english_")
                                )
                            )
                    )).Map<T>(m =>
                        m.AutoMap<T>()
                            .Properties(p => p
                                .Text(t => t
                                    .Name(n => n.Name)
                                    .Analyzer("index_question")
                                    .SearchAnalyzer("search_question")
                                    .SearchQuoteAnalyzer("search_question")
                                )
                            )
                        )
                    );
                    return createIndexResponse;

                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Creating Index failed");
                    throw ex;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
