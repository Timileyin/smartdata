﻿using ElasticSearch.Repository.DTOs;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElasticSearch.Repository.Base
{
    public abstract class BaseRepository<T> where T : class
    {
        protected readonly ElasticClient _client;
        public BaseRepository(ElasticClient client)
        {
            _client = client;
        }
        public abstract Task CreateObject(T obj);
        public abstract Task<BulkResponse> CreateObjects(List<T> obj);
        public abstract List<T> SearchIndex();
        public async Task<List<MultiIndexSearchDTO>> SearchAllIndices(string searchParam, List<string> market, int limit)
        {

            var response = await _client.SearchAsync<MultiIndexSearchDTO>(s =>
                                            s.Index("app_*")
                                            .Size(limit)
                                            .Query(q =>
                                            q.QueryString(m => m.DefaultField(f => f.Name).Query(searchParam)) 
                                            && q.Match(m=>m.Field(f=>f.Market).Query(string.Join(',',market)))));
            return response.Documents.ToList();
        }
    }
}
