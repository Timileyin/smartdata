﻿using ElasticSearch.Data.Entities;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ElasticSearch.Repository.Base
{
    public class Repository<T> : BaseRepository<T> where T : BaseEntity
    {
        public Repository(ElasticClient client):base(client)
        {

        }
        public override Task CreateObject(T obj)
        {
            throw new NotImplementedException();
        }

        public async override Task<BulkResponse> CreateObjects(List<T> documents)
        {
            var indexResponse = await _client.IndexManyAsync<T>(documents, $"app_{typeof(T).Name.ToLower()}");
            return indexResponse;
        }

        public override List<T> SearchIndex()
        {
            throw new NotImplementedException();
        }
    }
}
