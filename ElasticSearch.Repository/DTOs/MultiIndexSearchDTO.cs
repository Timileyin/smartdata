﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearch.Repository.DTOs
{
    public class MultiIndexSearchDTO
    {
        public string Market { get; set; }
        public string State { get; set; }
        public int? PropertyID { get; set; }
        public int? MgmtID { get; set; }
        public string Name { get; set; }
        public string FormerName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string Type
        {
            get { return PropertyID == null && MgmtID != null ? "Mgmt" : "Property"; }
            set { }
        }


    }
}
